file(GLOB_RECURSE sources CONFIGURE_DEPENDS
    src/*.c
    src/*.h
    include/*.h
)

add_executable(image-transformer ${sources}
        include/bmp_io.h
        include/image.h
        include/transform.h
        src/bmp_io.c
        src/image.c
        src/transform.c
        include/bmp_header.h
        include/pixel.h
        include/messages.h
        include/error_codes.h)
target_include_directories(image-transformer PRIVATE src include)
