#ifndef BMP_HEADER_H
#define BMP_HEADER_H


#include <stdint.h>

// Определение структуры заголовка BMP
struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;  // Тип файла
    uint32_t bfileSize;  // Размер файла
    uint32_t bfReserved;  // Зарезервировано
    uint32_t bOffBits;  // Смещение до начала данных изображения
    uint32_t biSize;  // Размер заголовка информации о изображении
    uint32_t biWidth;  // Ширина изображения
    uint32_t biHeight;  // Высота изображения
    uint16_t biPlanes;  // Количество плоскостей
    uint16_t biBitCount;  // Количество бит на пиксель
    uint32_t biCompression;  // Тип сжатия
    uint32_t biSizeImage;  // Размер изображения
    uint32_t biXPelsPerMeter;  // Горизонтальное разрешение
    uint32_t biYPelsPerMeter;  // Вертикальное разрешение
    uint32_t biClrUsed;  // Количество используемых цветов
    uint32_t biClrImportant;  // Количество важных цветов
};

#endif
