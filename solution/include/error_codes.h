#ifndef ERROR_CODES_H
#define ERROR_CODES_H

typedef enum {
    SUCCESS,
    FAILURE,
    FILE_ERROR,
    INCORRECT_ANGLE
} ErrorCode;

#endif // ERROR_CODES_H
