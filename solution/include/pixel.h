#ifndef PIXEL_H
#define PIXEL_H

#include <stdint.h>

// Структура для представления пикселя в формате RGB
struct __attribute__((packed)) pixel {
    uint8_t b, g, r;  // Компоненты цвета: синий, зеленый и красный
};
#endif
