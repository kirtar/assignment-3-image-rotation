#ifndef BMP_IO_H
#define BMP_IO_H

#include "image.h"
#include <stdio.h>

// Определение статусов чтения BMP
enum read_status {
  READ_OK = 0,            // Чтение прошло успешно
  READ_INVALID_SIGNATURE, // Неверная сигнатура
  READ_INVALID_BITS,      // Неверное количество бит
  READ_INVALID_HEADER,    // Неверный заголовок
  READ_INVALID_FILE, // Файл не существует, или не может быть открыт
};

// Определение статусов записи BMP
enum write_status {
  WRITE_OK = 0, // Запись прошла успешно
  WRITE_ERROR,  // Ошибка записи
  WRITE_INVALID_FILE, // Файл не может быть открыт для записи
};

// Объявление функции для чтения из BMP-файла
enum read_status from_bmp(FILE *in, struct image **img);

// Объявление функции для записи в BMP-файл
enum write_status to_bmp(FILE *out, struct image const *img);

#endif
