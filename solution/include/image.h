#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>
#include <stdlib.h>

#include "pixel.h"

// Структура для представления изображения
struct image {
  uint64_t width, height; // Ширина и высота изображения в пикселях
  struct pixel *data; // Массив пикселей изображения
};

// Функция для создания нового изображения с заданными шириной и высотой
struct image *create_image(uint64_t width, uint64_t height);

// Функция для удаления изображения и освобождения занимаемой им памяти
void destroy_image(struct image *img);

#endif
