#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "image.h"

enum transform_status {
    TRANSFORM_OK = 0,
    TRANSFORM_INVALID_IMAGE,
    TRANSFORM_ALLOCATION_FAILED
};

enum transform_status rotate(struct image* img, int angle);

#endif
