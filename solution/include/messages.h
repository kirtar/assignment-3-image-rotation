#ifndef MESSAGES_H
#define MESSAGES_H

#define USAGE "Использование: ./image-transformer <source-image> <transformed-image> <angle>\n"
#define READ_ERROR "Ошибка при чтении файла %s: %d\n"
#define ROTATE_ERROR "Ошибка при повороте изображения: %d\n"
#define WRITE_ERROR "Ошибка при записи файла %s: %d\n"
#define FILE_OPEN_ERROR "Не удалось открыть файл %s для %s.\n"
#define ANGLE_ERROR "Угол должен быть одним из следующих значений: 0, 90, -90, 180, -180, 270, -270.\n"

#endif // MESSAGES_H
