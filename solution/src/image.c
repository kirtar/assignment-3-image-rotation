#include "image.h"
#include <string.h>

struct image *create_image(uint64_t const width, uint64_t const height) {
  struct image *img = malloc(sizeof(struct image));
  if (img == NULL) {
    return NULL;
  }
  img->width = width;
  img->height = height;
  img->data = malloc(width * height * sizeof(struct pixel));
  if (img->data == NULL) {
    free(img);
    return NULL;
  }
  // Инициализируем выделенную память
  memset(img->data, 0, width * height * sizeof(struct pixel));
  return img;
}

void destroy_image(struct image *img) {
  if (img != NULL) {
    free(img->data);
    free(img);
  }
}
