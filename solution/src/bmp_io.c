#include "bmp_io.h"
#include "bmp_header.h"

#define BMP_TYPE 0x4D42
#define BMP_BITS_PER_PIXEL 24
#define BMP_INFO_HEADER_SIZE 40

uint64_t calculate_padding(uint64_t width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

enum read_status read_image_data(FILE* in, struct image* img) {
    uint64_t padding = calculate_padding(img->width);
    uint8_t pad[3];
    for (uint64_t y = 0; y < img->height; ++y) {
        if (fread(img->data + y * img->width, sizeof(struct pixel), img->width, in) != img->width) {
            return READ_INVALID_BITS;
        }
        if (padding > 0 && fread(pad, 1, padding, in) != padding) {
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

enum write_status write_image_data(FILE* out, struct image const* img) {
    uint64_t padding = calculate_padding(img->width);
    uint8_t pad[3] = {0, 0, 0};
    for (uint64_t y = 0; y < img->height; ++y) {
        if (fwrite(img->data + y * img->width, sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_ERROR;
        }
        if (padding > 0 && fwrite(pad, 1, padding, out) != padding) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

int is_valid_bmp_header(struct bmp_header* header) {
    return header->bfType == BMP_TYPE && header->biBitCount == BMP_BITS_PER_PIXEL && header->biSize == BMP_INFO_HEADER_SIZE && header->biPlanes == 1 && header->biCompression == 0;
}

enum read_status from_bmp(FILE* in, struct image** img) {
    if (in == NULL) {
        return READ_INVALID_FILE;
    }

    struct bmp_header header;
    if (fread(&header, sizeof(header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }
    if (!is_valid_bmp_header(&header)) {
        return READ_INVALID_SIGNATURE;
    }

    *img = create_image(header.biWidth, header.biHeight);
    if (*img == NULL) {
        return READ_INVALID_FILE;
    }
    return read_image_data(in, *img);
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    if (out == NULL) {
        return WRITE_INVALID_FILE;
    }

    struct bmp_header header;
    header.bfType = BMP_TYPE;
    header.bfileSize = img->width * img->height * sizeof(struct pixel) + sizeof(header) + img->height * calculate_padding(img->width);
    header.bfReserved = 0;
    header.bOffBits = sizeof(header);
    header.biSize = BMP_INFO_HEADER_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = BMP_BITS_PER_PIXEL;
    header.biCompression = 0;
    header.biSizeImage = img->width * img->height * sizeof(struct pixel);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    if (fwrite(&header, sizeof(header), 1, out) != 1) {
        return WRITE_ERROR;
    }
    return write_image_data(out, img);
}











