#include "transform.h"
#include <string.h>

enum transform_status rotate(struct image *img, int angle) {
  if (img == NULL || img->data == NULL) {
    return TRANSFORM_INVALID_IMAGE;
  }

  struct pixel **temp = malloc(img->height * sizeof(struct pixel *));
  if (temp == NULL) {
    return TRANSFORM_ALLOCATION_FAILED;
  }
  for (uint64_t y = 0; y < img->height; ++y) {
    temp[y] = malloc(img->width * sizeof(struct pixel));
    if (temp[y] == NULL) {
      for (uint64_t i = 0; i < y; ++i) {
        free(temp[i]);
      }
      free(temp);
      return TRANSFORM_ALLOCATION_FAILED;
    }
  }

  for (uint64_t y = 0; y < img->height; ++y) {
    for (uint64_t x = 0; x < img->width; ++x) {
      temp[y][x] = img->data[y * img->width + x];
    }
  }

  struct pixel *new_data =
      malloc(img->width * img->height * sizeof(struct pixel));
  if (new_data == NULL) {
    for (uint64_t y = 0; y < img->height; ++y) {
      free(temp[y]);
    }
    free(temp);
    return TRANSFORM_ALLOCATION_FAILED;
  }

  if (angle == 0) {
    memcpy(new_data, img->data,
           img->width * img->height * sizeof(struct pixel));
  } else if (angle == 90 || angle == -270) {
    for (uint64_t y = 0; y < img->height; ++y) {
      for (uint64_t x = 0; x < img->width; ++x) {
        new_data[x * img->height + y] = temp[y][img->width - x - 1];
      }
    }
  } else if (angle == -90 || angle == 270) {
    for (uint64_t y = 0; y < img->height; ++y) {
      for (uint64_t x = 0; x < img->width; ++x) {
        new_data[x * img->height + (img->height - y - 1)] = temp[y][x];
      }
    }
  } else { // angle == 180 or angle == -180
    for (uint64_t y = 0; y < img->height; ++y) {
      for (uint64_t x = 0; x < img->width; ++x) {
        new_data[(img->height - y - 1) * img->width + (img->width - x - 1)] =
            temp[y][x];
      }
    }
  }

  for (uint64_t y = 0; y < img->height; ++y) {
    free(temp[y]);
  }
  free(temp);

  free(img->data);
  img->data = new_data;
  if (angle == 90 || angle == -90 || angle == 270 || angle == -270) {
    uint64_t tmp = img->width;
    img->width = img->height;
    img->height = tmp;
  }

  return TRANSFORM_OK;
}
