#include "bmp_io.h"
#include "error_codes.h"
#include "messages.h"
#include "transform.h"


#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc != 4) {
        printf(USAGE);
        return FAILURE;
    }

    FILE *in = fopen(argv[1], "rb");
    if (in == NULL) {
        printf(FILE_OPEN_ERROR, argv[1], "чтения");
        return FILE_ERROR;
    }

    struct image *img;
    enum read_status read_status = from_bmp(in, &img);
    fclose(in);
    if (read_status != READ_OK) {
        printf(READ_ERROR, argv[1], read_status);
        return FILE_ERROR;
    }

    int angle = atoi(argv[3]);
    if (angle != 0 && angle != 90 && angle != -90 && angle != 180 &&
        angle != -180 && angle != 270 && angle != -270) {
        printf(ANGLE_ERROR);
        destroy_image(img);
        return INCORRECT_ANGLE;
    }

    enum transform_status transform_status = rotate(img, angle);
    if (transform_status != TRANSFORM_OK) {
        printf(ROTATE_ERROR, transform_status);
        destroy_image(img);
        return FILE_ERROR;
    }

    FILE *out = fopen(argv[2], "wb");
    if (out == NULL) {
        printf(FILE_OPEN_ERROR, argv[2], "записи");
        destroy_image(img);
        return FILE_ERROR;
    }

    enum write_status write_status = to_bmp(out, img);
    fclose(out);
    if (write_status != WRITE_OK) {
        printf(WRITE_ERROR, argv[2], write_status);
        destroy_image(img);
        return FILE_ERROR;
    }

    destroy_image(img);
    return SUCCESS;
}
